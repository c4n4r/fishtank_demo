extends CharacterBody2D
class_name Player

@onready var rc_down := $RC_Down
@onready var dbl_jump_timer := $DoubleJumpTimer
@onready var sprite := $AnimatedSprite2D

@export var SPEED = 300.0
@export var JUMP_VELOCITY = -400.0

var direction: float
var jump_count = 0
var can_double_jump := false

# Get the gravity from the project settings to be synced with RigidBody nodes.
var DEFAULT_GRAV = ProjectSettings.get_setting("physics/2d/default_gravity")
var gravity
func _physics_process(delta):
	gravity = 0.0 if is_on_floor() else DEFAULT_GRAV
	velocity.y += gravity * delta
	if rc_down.is_colliding():
		can_double_jump = false
		jump_count = 0
	move_and_slide()

	
func move() -> void :
	if(direction == -1):
		sprite.flip_h = true
	if(direction == 1):
		sprite.flip_h = false
	velocity.x = direction * SPEED

func make_jump() -> void :
	dbl_jump_timer.start()
	jump_count = jump_count + 1
	velocity.y = JUMP_VELOCITY if is_on_floor() else JUMP_VELOCITY 

func can_bouble_jump() -> bool:
	return can_double_jump and jump_count < 1

func define_direction(dir):
	direction = dir
	


func _on_double_jump_timer_timeout():
	can_double_jump = true
