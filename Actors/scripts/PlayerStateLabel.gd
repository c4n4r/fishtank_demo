extends Label

var sm: StateMachine
func _ready():
	sm = get_parent().get_node('StateMachine')
	
func _process(delta):
	if sm:
		text = sm.actual_state.name +" " +str(get_parent().can_bouble_jump())
