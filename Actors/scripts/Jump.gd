extends State


func _logic(): 
	body.define_direction(direction)
	if (body.is_on_floor() and body.velocity.y == 0):
		next_state = states['Idle']
	if body.velocity.y >   0:
		next_state = states['Fall']
	if jump: 
		if (body.can_bouble_jump()):
			body.make_jump()

	
func _enter():
	super()
	body.make_jump()


