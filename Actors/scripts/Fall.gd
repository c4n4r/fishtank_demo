extends State

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _logic():
	body.define_direction(direction)
	if(jump and body.can_bouble_jump()):
		next_state = states['Jump']
	if(body.is_on_floor()):
		next_state = states['Idle']
