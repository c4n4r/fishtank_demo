extends Node
class_name State

@onready var states: Dictionary = {}
var body: Player
var next_state: State

#inputs
var direction := 0.0
var crouch := false
var jump := false

func _enter() -> void :
	body.sprite.play(self.name)
	next_state = null
	
func _base_logic() ->void :
	get_inputs()
	body.move()
	
func _get_transition():
	return next_state
	
func get_inputs():
	direction = Input.get_axis('ui_left', 'ui_right')
	jump = Input.is_action_just_pressed("ui_accept")
	crouch = Input.is_action_pressed("ui_down")
	
