extends Node
class_name StateMachine

@export var initial_state: String 
var states: Dictionary
var actual_state = State

func _ready():
	await get_parent().ready
	for state in get_child(0).get_children():
		self.states[state.name] = state

	for state in get_child(0).get_children():
		state.states = self.states
		state.body = owner
		print(state.states)
	actual_state = states[initial_state]
	
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	actual_state._base_logic()
	actual_state._logic()
	if(actual_state._get_transition() != null):
		change_state(actual_state.next_state)

func change_state(new_state: State) -> void :
	actual_state = new_state
	new_state._enter()



