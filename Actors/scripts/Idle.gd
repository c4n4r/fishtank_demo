extends State

func _logic() -> void :
	if (not body.is_on_floor() and body.velocity.y > 0):
		next_state = states['Fall']
	if(not crouch and direction != 0.0):
		next_state = states['Walk']
	if(jump):
		next_state = states['Jump']
	if(crouch):
		next_state = states['Crouch']
		
func _enter() -> void :
	super()

	
